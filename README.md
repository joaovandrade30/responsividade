# Responsividade


### CSS Units

- Unidades de medida do CSS
  - Layout fixo:
    - Px  
  - Layout fluído:
    - % 
    - auto -> Automática
    - vh -> Viewport: height e width
  - Textos fixos:
    - 1px = 0.75pt
    - 16px = 12pt
  - Textos fluídos:
    - em -> multiplicado pelo pai
    - rem -> multiplicado pelo root
